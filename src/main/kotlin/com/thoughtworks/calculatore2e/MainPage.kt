package com.thoughtworks.calculatore2e

import com.thoughtworks.calculatore2e.extensions.performHorizontalSwipe
import io.appium.java_client.AppiumDriver
import io.appium.java_client.MobileElement
import io.appium.java_client.pagefactory.AndroidFindBy
import io.appium.java_client.pagefactory.AppiumFieldDecorator
import org.openqa.selenium.support.PageFactory


class MainPage(private val driver: AppiumDriver<MobileElement>) {

    @AndroidFindBy(id = "digit_2")
    lateinit var buttonTwo: MobileElement

    @AndroidFindBy(id = "digit_3")
    lateinit var buttonThree: MobileElement

    @AndroidFindBy(id = "digit_4")
    lateinit var buttonFour: MobileElement

    @AndroidFindBy(id = "result")
    lateinit var result: MobileElement

    @AndroidFindBy(accessibility = "plus")
    lateinit var buttonPlus: MobileElement

    @AndroidFindBy(accessibility = "equals")
    lateinit var buttonEquals: MobileElement

    init {
        PageFactory.initElements(AppiumFieldDecorator(driver), this)
    }

    fun simpleSum() {
        buttonTwo.click()
        buttonThree.click()

        buttonPlus.click()

        buttonFour.click()
        buttonThree.click()

        buttonEquals.click()
    }

    fun squareRoot() {
        openSpecialFunctions()
        buttonPlus.click()
        closeSpecialFunctions()
        buttonFour.click()

        buttonEquals.click()
    }

    fun getResult(): String = result.text

    private fun openSpecialFunctions() {
        driver.performHorizontalSwipe(0.99, 0.2)
    }

    private fun closeSpecialFunctions() {
        driver.performHorizontalSwipe(0.2, 0.99)
    }
}
