package com.thoughtworks.calculatore2e.extensions

import io.appium.java_client.AppiumDriver
import io.appium.java_client.MobileElement
import io.appium.java_client.android.AndroidTouchAction
import io.appium.java_client.touch.offset.PointOption

fun AppiumDriver<MobileElement>.performHorizontalSwipe(startX: Double, endX: Double) {
    val size = manage().window().size

    val startPoint = PointOption.point((size.width * startX).toInt(), size.height / 2)
    val endPoint = PointOption.point((size.width * endX).toInt(), size.height / 2)

    AndroidTouchAction(this)
        .press(startPoint)
        .moveTo(endPoint)
        .release()
        .perform()
}