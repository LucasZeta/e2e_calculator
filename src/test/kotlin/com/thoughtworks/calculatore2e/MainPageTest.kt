package com.thoughtworks.calculatore2e

import com.thoughtworks.calculatore2e.util.AppiumTest
import org.junit.Assert.assertEquals
import org.junit.Test

class MainPageTest : AppiumTest() {

    @Test
    fun simpleSum() {
        val page = MainPage(requireDriver())

        page.simpleSum()
        assertEquals("66", page.getResult())
    }

    @Test
    fun squareRoot() {
        val page = MainPage(requireDriver())

        page.squareRoot()
        assertEquals("2", page.getResult())
    }
}