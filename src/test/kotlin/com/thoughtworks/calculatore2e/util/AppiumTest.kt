package com.thoughtworks.calculatore2e.util

import io.appium.java_client.AppiumDriver
import io.appium.java_client.MobileElement
import io.appium.java_client.android.AndroidDriver
import io.appium.java_client.remote.AndroidMobileCapabilityType
import io.appium.java_client.remote.MobileCapabilityType
import org.junit.After
import org.junit.Before
import org.openqa.selenium.remote.DesiredCapabilities
import java.util.concurrent.TimeUnit

abstract class AppiumTest {

    private var driver: AppiumDriver<MobileElement>? = null

    @Before
    fun setUp() {
        val desiredCapabilities = DesiredCapabilities().apply {
            setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554")
            setCapability(MobileCapabilityType.PLATFORM_NAME, "Android")
            setCapability(MobileCapabilityType.NO_RESET, true)
            setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.android.calculator2")
            setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "Calculator")
        }

        driver = AndroidDriver<MobileElement>(desiredCapabilities)

        driver?.let {
            it.manage()?.timeouts()?.implicitlyWait(30, TimeUnit.SECONDS)
        }
    }

    @After
    fun tearDown() {
        driver?.quit()
    }

    protected fun requireDriver(): AppiumDriver<MobileElement> {
        return driver ?: throw IllegalStateException("driver not started")
    }
}